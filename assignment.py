#!/usr/bin/python
#python script for PY3C01 assignment
#using the Metropolis algorithm for the Ising model

import numpy as np
from numpy import random 
from random import randint, random, choice
import matplotlib.pyplot as plt
import matplotlib as mpl
import math
from pylab import imshow, show, get_cmap
from PIL import Image


J=1 # exchange energy,interation strength
k=1 # Boltzmann constant
n=31 # number of rows
m=31 # number of columns
s=(1, -1) # spin states
h=0 # external magnetic field strength
iterations=1000
magnetizations = []
Temps=[]
energies=[]
specificheat=[]


def make_lattice():
   lattice = np.empty(shape=(n,m)) # making an empty nxm lattice
   for x in range(n):
      for y in range(m):
         lattice[x][y] = choice(s) # putting spin states into the lattice randomly

   for x in range(n):
      lattice[x][m-1] = lattice[x][0] # last coloumn equal to the first
   for y in range(m):
      lattice[n-1][y] = lattice[0][y] # last row equal to the first
   return lattice
 

# function that flips spin states
def flip_spin(i, j, lattice): 
   if lattice[i][j] == 1:  
      lattice[i][j] = -1  # if the spin at site [i,j]=1, flip to -1
   else:
      lattice[i][j] = 1  # else the spin is -1, flip to +1
   return lattice


# function that gives change in energy from flipping spin state
def energy_change(i, j, h, lattice):
   if lattice[i][j] == 1:
      energy_change = 2*(J*(lattice[(i+1)%n][j] + lattice[i][(j-1)%m] + lattice[i][(j+1)%m] + lattice[(i-1)%n][j]) + h)
   else:
      energy_change = -2*(J*(lattice[(i+1)%n][j] + lattice[i][(j-1)%m] + lattice[i][(j+1)%m] + lattice[(i-1)%n][j]) + h)
   return energy_change
   


for T in np.arange(0.1, 5, 0.1):

   lattice=make_lattice() # create lattice of spins
   Temps.append(T)
   energy=0
   magnetization=0
   beta= 1.0 / T
   e1=e2=0

   # metropolis algorithm
   for iteration in range(iterations):
      for i in range(n):  # sweeping through the lattice
         for j in range(m):

            if energy_change(i, j, h, lattice) < 0: # flip the spin if the energy change is less than 0
               flip_spin(i, j, lattice)
         
            elif random() < math.exp(-beta*energy_change(i, j, h, lattice)): # flip the spin with probability exp(-beta*energy_change)
               flip_spin(i, j, lattice)
          
               
            S=lattice[i][j]
            nb=lattice[(i+1)%n][j] + lattice[i][(j-1)%m] + lattice[i][(j+1)%m] + lattice[(i-1)%n][j] + h
            energy=energy-2*J*nb*S  # adding the energy at each site
            e1=e1+energy        
            e2=e2+energy*energy     # energies for specific heat

    #plotting lattice, change temperature step to 1 when plotting these
      #if iteration==5 or iteration==10 or iteration==1000:
       #  imshow(lattice, cmap=get_cmap("Spectral"), interpolation='nearest')
        # plt.title('Spin States, Iteration = %d, Temperature = %d, J=%d, h=%d'%(iteration, T, J, h))
      #plt.show()
     

   energies.append(energy/(n*m*iterations)) # average energy per site
   specificheat.append((e2/(iterations*n*m) + (e1*e1)/(iterations*iterations*n*n*m*m)) *(beta * beta)) # specific heat
   magnetization = abs(np.sum(lattice)/(n*m)) # average magnetization per site
   magnetizations.append(magnetization)
   
   

      
f = plt.figure(figsize=(12, 8)); # plot the calculated values   
 
sp =  f.add_subplot(2, 2, 1 );
plt.plot(np.array([Temps]), np.array([magnetizations]), 'bo')   
plt.title('M vs T, J=%d, H=%d'%(J,h))
plt.xlabel('Temperature')
plt.ylabel('Magnetization')   
  

sp = f.add_subplot(2, 2, 2);
plt.plot(np.array([Temps]), np.array([energies]), 'bo')
plt.title('E vs T, J=%d, H=%d'%(J,h))
plt.xlabel('Temperature')
plt.ylabel('Energy')

sp = f.add_subplot(2, 2, 3);
plt.plot(np.array([Temps]), np.array([specificheat]), 'bo') # not returning correct plot
plt.title('Specific Heat vs T, J=%d, H=%d'%(J,h))
plt.ylabel('Specific Heat')
plt.xlabel('Temperature')

#plt.savefig('nplot_h=0_J=1.png')

plt.tight_layout()
plt.show()







  
